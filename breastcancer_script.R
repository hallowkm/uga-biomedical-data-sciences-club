#Load libraries (may have to install first using install.packages
library(lattice)
library(ggplot2)

#Read in data - update directory to directory where you stored the data. Make sure to use forward slash, not backslash
A=read.table("C:/Users/hallowkm/Desktop/temp/MHEALTHDATASET/breastcancer.csv", header=T, sep = ",")


#Lattice plot example - unrelated to the rest of this script.
#Uses built-in dataset Theoph
xyplot(conc~Time | factor(paste("Dose: ", Dose)), group = Subject, Theoph, type = "b", layout=c(5,2), xlab="Time (min)", ylab = "Conc (ng/ml)", lwd=2, main = "Drug Exposure")

#GGplot example using loaded data: 
ggplot(A) + geom_point(mapping = aes(x = I0, y = A_DA, color = Class),size=1.5) + scale_color_discrete(name="Class")+
  xlab("Impedance") + 
  ylab("Area")+
  ggtitle("Breast Cancer Tissue Properties")


#Some examples of initial data exploration
head(A)
names(A)
dim(A)
summary(A)

histogram(~I0 | Class, A)

xyplot(I0~PA500, A)

xyplot(I0~DA, group = Class, A, auto.key = T)

xyplot(I0~DA | Class, A, auto.key = T)



ggplot(A) + geom_point(mapping = aes(x = DA, y = I0, color = Class))


classlist = unique(A$Class)
classes = data.frame(Class = classlist, fullClass = c("Carcinoma", "Fibro-adenoma", "Mastopathy", "Glandular", "Connective", "Adipose"))

A = merge(A, classes, by = "Class")
ggplot(A) + geom_point(mapping = aes(x = DA, y = I0, color = fullClass)) 

ggplot(A) + geom_point(mapping = aes(x = I0, y = A_DA, color = fullClass),size=1.5) + scale_color_discrete(name="Class")+xlab("Impedance") + ylab("Area")+ggtitle("Breast Cancer Tissue Properties")


# + facet_wrap(~ fullClass, nrow = 2) + xlabel(")+ guides(fill=guide_legend(title=NULL))
+ theme(legend.title=element_blank())